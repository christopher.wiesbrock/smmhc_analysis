# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 14:28:55 2024

@author: wiesbrock
"""

#import packages

import numpy as np
import pandas as pd
import scipy.stats as stats
import scipy.signal as signal
import matplotlib.pylab as plt
import seaborn as sns
from pybaselines import Baseline
from pybaselines.utils import gaussian
from sklearn.preprocessing import MinMaxScaler


def find_number_sequences(zeitreihe):
    start_number_seq = []
    end_number_seq = []

    in_number_seq = False
    for idx, val in enumerate(zeitreihe):
        if not np.isnan(val):
            if not in_number_seq:
                start_number_seq.append(idx)
                in_number_seq = True
        else:
            if in_number_seq:
                end_number_seq.append(idx)
                in_number_seq = False

    # Wenn die letzte Sequenz eine Zahlensequenz ist, muss ihr Ende hinzugefügt werden
    if in_number_seq:
        end_number_seq.append(len(zeitreihe))

    return start_number_seq, end_number_seq


def find_nan_sequences(zeitreihe):
    is_nan = np.isnan(zeitreihe)
    start_nan_seq = []
    end_nan_seq = []

    in_nan_seq = False
    for idx, val in enumerate(is_nan):
        if val and not in_nan_seq:
            start_nan_seq.append(idx)
            in_nan_seq = True
        elif not val and in_nan_seq:
            end_nan_seq.append(idx)
            in_nan_seq = False

    # Falls die letzte Sequenz bis zum Ende des Arrays reicht
    if in_nan_seq:
        end_nan_seq.append(len(zeitreihe))

    return np.array(start_nan_seq), np.array(end_nan_seq)

def adjust_evaluation(evaluation):
    # Berechnen der Differenz
    evaluation_diff = np.diff(evaluation)

    # Durchlaufen von evaluation_diff
    for i in range(len(evaluation_diff) - 1):
        # Überprüfen auf [1, -1] oder [-1, 1]
        if (evaluation_diff[i] == 1 and evaluation_diff[i + 1] == -1) or (evaluation_diff[i] == -1 and evaluation_diff[i + 1] == 1):
            # Setzen der entsprechenden Werte in evaluation auf 0
            evaluation[i] = 0
            evaluation[i + 1] = 0

    return evaluation

path_groundtruth=r"C:\Users\Chris\Downloads\Ground-Truth-SMMHC-20240304T091148Z-001\Ground-Truth-SMMHC\Ground truth.xlsx"
path_green=r"C:\Users\Chris\Downloads\Ground-Truth-SMMHC-20240304T091148Z-001\Ground-Truth-SMMHC\Gesamt_RawGre.xlsx"
path_red=r"C:\Users\Chris\Downloads\Ground-Truth-SMMHC-20240304T091148Z-001\Ground-Truth-SMMHC\gesamt_RawRed.xlsx"

green_data=pd.read_excel(path_green)
names=green_data.columns

names=names[1:]
z_green=green_data.copy()
bg_corrected=green_data.copy()

red_data=pd.read_excel(path_red)
groundtruth_data=pd.read_excel(path_groundtruth)





count_all=0
sensitivity_all=np.zeros((4))
ppv_all=np.zeros((4))
deviance_all=np.zeros((4))
#indices_frame = pd.DataFrame(columns=names)
indices_frame = pd.DataFrame(columns=names, index=np.arange(200))


#indices_frame.columns=names
print(indices_frame)
for m in [0.2,0.21,0.22,0.23]:
    sensitivity=np.zeros((len(names)))
    ppv=np.zeros((len(names)))
    deviance=np.zeros((len(names)))
    count=0
    for i in names:
        z_green[i] = stats.zscore(green_data[i], nan_policy='omit')
        #z_green[i] = z_green[i].dropna()

        x = np.linspace(0, len(green_data[i]), len(green_data[i]))
        baseline_fitter = Baseline(x_data=x)
        corrected_data = green_data[i].copy()

        nan_start_indices, nan_end_indices = find_nan_sequences(corrected_data)

        baseline_data = np.nan_to_num(corrected_data, nan=np.mean(corrected_data))
        baseline_green = baseline_fitter.noise_median(baseline_data)[0]

        complete_data = corrected_data - baseline_green
        curated = complete_data.copy() # Setzen des gesamten Arrays auf NaN

        # Erweiterung der NaN-Bereiche um 20 Frames und Setzen auf NaN
        for start, end in zip(nan_start_indices, nan_end_indices):

            # Erweiterung der Start- und Endindizes um 20 Frames unter Berücksichtigung der Array-Grenzen
            #extended_start = max(start - 100, 0)
            #extended_end = min(end + 100, len(complete_data))



            # Setzen des erweiterten Bereichs auf NaN
            curated[start-40:end+40] = np.nan

        start_seq, end_seq = find_number_sequences(curated)

        seq_check=np.ones((len(start_seq))).astype(int)

        for k in range(len(end_seq)):
            if end_seq[k]-start_seq[k]<len(curated)*0.1:
                curated[start_seq[k]:end_seq[k]+1]=np.nan
                seq_check[k]=0
            if len(end_seq)>1 and start_seq[k]!=0 and end_seq[k]!=len(end_seq):
                curated[start_seq[k]-5:start_seq[k]]=np.nan
                curated[end_seq[k]:start_seq[k]+5]=np.nan



        y=np.zeros((len(corrected_data)))
        peaks=groundtruth_data[i].dropna()
        peaks=peaks.astype(int)
        #peaks=peaks-1
        y[peaks]=1

        for start, end in zip(nan_start_indices, nan_end_indices):

            # Erweiterung der Start- und Endindizes um 20 Frames unter Berücksichtigung der Array-Grenzen
            #extended_start = max(start - 100, 0)
            #extended_end = min(end + 100, len(complete_data))



            # Setzen des erweiterten Bereichs auf NaN
            y[start-40:end+40] = np.nan

        for k in range(len(end_seq)):
            if end_seq[k]-start_seq[k]<len(curated)*0.1:
                y[start_seq[k]:end_seq[k]+1]=np.nan
            if len(end_seq)>1 and start_seq[k]!=0 and end_seq[k]!=len(end_seq):
                y[start_seq[k]-5:start_seq[k]]=np.nan
                y[end_seq[k]:start_seq[k]+5]=np.nan

        corrected_data=np.nan_to_num(curated,nan=0)
        
        corrected_data=stats.zscore(corrected_data)
        
        corrected_data[:] = (corrected_data[:] - corrected_data[:].min()) / (corrected_data[:].max() - corrected_data[:].min())
        


        start_seq=start_seq[np.where(seq_check==1)[0][0]]
        end_seq=end_seq[np.where(seq_check==1)[0][0]]

        #max_len = len(end_seq)

        binary=np.zeros((len(corrected_data)))
        peak_indices=signal.find_peaks(corrected_data, prominence=0.2, threshold=0.22)[0]
        #print(peak_indices)
        #binary[corrected_data>3.]=1
        binary[peak_indices]=1
        start_peak=np.diff(binary)
        start_peak[start_peak!=1]=0
        
        peak_series=pd.Series(peak_indices)
        indices_frame[i] = peak_series



        freq_list=[]
        '''
        for o in range(len(start_seq)):
            if np.sum(start_peak[start_seq[o]:end_seq[o]])>1:
                first_peak=np.where(start_peak[start_seq[o]:end_seq[o]]==1)[0][0]
                last_peak=np.where(start_peak[start_seq[o]:end_seq[o]]==1)[0][-1]
            if np.sum(start_peak[start_seq[o]:end_seq[o]])<=1:
                first_peak=start_seq[o]
                last_peak=end_seq[o]
            freq_list.append(((np.sum(start_peak[start_seq[o]:end_seq[o]])/(last_peak-first_peak))/2.3)*60)
        '''

        #data_dict[m] = freq_list



        #plt.figure()
        #plt.plot(x,y)
        
        corrected_data=green_data[i]

        plt.figure(dpi=300)

        x=np.linspace(0,len(corrected_data),len(corrected_data))
        plt.subplot(211)
        plt.title(str(i)+'Algorithm')
        plt.plot(x,corrected_data,'r')
        plt.plot(x[peak_indices],corrected_data[peak_indices],'bo')
        plt.xlim(0,len(corrected_data))
        plt.xticks([])
        
        plt.subplot(212)
        plt.title(str(i)+ 'Ground truth')
        plt.plot(x,corrected_data,'r')
        plt.plot(x[peaks],corrected_data[peaks],'bo')
        plt.xlim(0,len(corrected_data))
        plt.xticks([])

        #plt.subplot(312)
        #plt.plot(base_corrected_data)
        #plt.xlim(0,len(base_corrected_data))
        
        #plt.subplot(212)
        #plt.plot(np.diff(corrected_data))
        #plt.plot(np.diff(corrected_data))
        #plt.xlim(0,len(binary))
        #plt.xticks([])

        #plt.subplot(412)
        #plt.plot(binary)
        #plt.xlim(0,len(binary))
        #plt.xticks([])
        '''
        plt.subplot(413)
        plt.plot(y)
        plt.xlim(0,len(binary))
        plt.xticks([])

        plt.subplot(414)
        plt.plot(binary-y)
        plt.xlim(0,len(binary))
        #plt.xticks([])
        '''

        '''
        plt.figure()
        plt.title(str(m))
        plt.subplot(311)
        plt.plot(binary)
        plt.xlim(0,len(binary))
        plt.subplot(312)
        plt.plot(y)
        plt.xlim(0,len(binary))
        plt.subplot(313)
        plt.plot(binary-y)
        plt.xlim(0,len(binary)
        '''
        
        evaluation=binary-y
        adjusted_evaluation=adjust_evaluation(evaluation)

        print(i)
        print(('GT:'+str(np.count_nonzero(y == 1))))
        print(('All detected:'+str(np.count_nonzero(binary == 1))))

        print(('True positive:'+str(np.count_nonzero(binary == 1)-(np.count_nonzero(binary-y == 1)))))

        print(('False Positive:'+str(np.count_nonzero(binary-y == 1)))) #False Pos
        print(('False Negative:'+str(np.count_nonzero(binary-y == -1)))) #False Neg

        print(('Sensitivity:'+ str((np.count_nonzero(binary == 1)-(np.count_nonzero(binary-y == 1)))/np.count_nonzero(y == 1))))
        print(('PPV:'+ str((np.count_nonzero(binary == 1)-(np.count_nonzero(binary-y == 1)))/np.count_nonzero(binary == 1))))

        plt.show()

        sensitivity[count]=(np.count_nonzero(binary == 1)-(np.count_nonzero(binary-y == 1)))/np.count_nonzero(y == 1)
        ppv[count]=(np.count_nonzero(binary == 1)-(np.count_nonzero(binary-y == 1)))/np.count_nonzero(binary == 1)
        deviance[count]=np.count_nonzero(binary == 1)/np.count_nonzero(y == 1)

        count=count+1
        
    sensitivity_all[count_all]=np.mean(sensitivity)
    ppv_all[count_all]=np.mean(ppv) 
    deviance_all[count_all]=np.mean(deviance)
    print('Mean Sens:'+str(np.mean(sensitivity)))
    print('Mean PPV:'+str(np.mean(ppv)))
    print('Mean Deviance:'+str(np.mean(deviance)))
    count_all=count_all+1

plt.figure()
plt.plot(sensitivity_all, label='Sensitivity')
plt.plot(ppv_all, label='PPV')
plt.xlabel('Prominence')
plt.ylabel('Sensitivity/PPV')
plt.legend()

plt.figure()
plt.plot(deviance_all)

indices_frame.to_excel(r'C:\Users\Chris\Downloads\Ground-Truth-SMMHC-20240304T091148Z-001\Ground-Truth-SMMHC\peaks.xlsx')
